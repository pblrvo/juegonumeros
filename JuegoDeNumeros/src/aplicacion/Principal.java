package aplicacion;

import java.util.Scanner;
import java.util.Random;


public class Principal{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduzca su nombre");
		String nombre_usuario = sc.next();
		int input_adecuado = 0;
		int numero_erroneo = 0;
		int intentos = 1;
		System.out.println(nombre_usuario + ", introduzca numero maximo");
		int numero_maximo = Integer.parseInt(sc.next());
		Random random = new Random();
		int ran = random.nextInt(numero_maximo);
		ran += 1;
		while(input_adecuado == 0 || numero_erroneo == 0){
			try{
			System.out.println(nombre_usuario + ", introduzca un numero");
			int guess = Integer.parseInt(sc.next());
			input_adecuado = 1;
				if(guess == ran){
					System.out.println(nombre_usuario + " has ganado!" + "\nIntentos: " + intentos );
					numero_erroneo = 1;	
				}else{
					if(guess > ran){
						System.out.println(nombre_usuario + ", el numero seleccionado es mayor que el generado. Intente de nuevo.");
					}else{
						System.out.println(nombre_usuario + ", el numero seleccionado es menor que el generado. Intente de nuevo.");
					}
					intentos += 1;
				}
			}catch (Exception e){
			System.out.println(nombre_usuario + " ,no ha introducido un numero");
			}
		}
	}
}	
